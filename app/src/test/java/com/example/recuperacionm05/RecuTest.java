package com.example.recuperacionm05;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RecuTest {

    @Test
    public void sumasUfs_test(){
        Matricula matricula = new Matricula("1","1","1","1","1","1","1","1","1",0);
        assertEquals(9, matricula.sumaUfs());

    }

    @Test
    public void calcPrecioMatricula(){
        //Bonificacion de un 0%
        Matricula matricula = new Matricula("1","12","1","1","1","1","1","1","1",1);
        assertEquals(360, matricula.calPrecioMatricula(), 0.1);

        //Bonificacion de un 50%
        matricula = new Matricula("1","12","1","1","1","1","1","1","1",0.5);
        assertEquals(180, matricula.calPrecioMatricula(), 0.1);

        //Bonificacion de un 100%
        matricula = new Matricula("1","12","1","1","1","1","1","1","1",0);
        assertEquals(0, matricula.calPrecioMatricula(), 0.1);

        //Comprobacion si se le inserta un numero vacio
        matricula = new Matricula("","","","","","","","","",1);
        assertEquals(0, matricula.calPrecioMatricula(), 0.1);

        //Comprobacion que no cobre cuando solo se matricula a fct
        matricula = new Matricula("","","","","","","","","1",1);
        assertEquals(0, matricula.calPrecioMatricula(), 0.1);

        //Solo se podria añadir valores negativos si se forzara el programa
        //No es podible añadir numero degativos, ni caractes especiales
    }
}
