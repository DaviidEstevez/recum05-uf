package com.example.recuperacionm05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public EditText m2,  m3,  m5, m6, m7, m8,  m9, m10, fct;

    public TextView totalPrice;

    public RadioButton boni1;
    public RadioButton boni2;

    Matricula matricula;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Inicializamos los editText*/
        m2 = findViewById(R.id.m2Price);
        m3 = findViewById(R.id.m3Price);
        m5 = findViewById(R.id.m5Price);
        m6 = findViewById(R.id.m6Price);
        m7 = findViewById(R.id.m7Price);
        m8 = findViewById(R.id.m8Price);
        m9 = findViewById(R.id.m9Price);
        m10 = findViewById(R.id.m10Price);
        fct = findViewById(R.id.fctPrice);
        boni1= findViewById(R.id.bonificacio1);
        boni2= findViewById(R.id.bonificacio2);


        /*Inicializamos TexView totalPrice*/
        totalPrice = findViewById(R.id.totalPrice);

        /*Inicializamos el boton y su listener*/
        Button btnCal = findViewById(R.id.btnCal);
        btnCal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /*Miramos que tipo de bonifiaçacion selecciono, por defecto se aplica un 0% de edescuento*/
                double bonificacion = 1;

                if(boni1.isChecked()){
                    bonificacion=0.5;
                }else if(boni2.isChecked()){
                    bonificacion=0;
                }
                
                /*Inicializamos matricula*/
                
                matricula = new Matricula(m2.getText().toString(),
                        m3.getText().toString(),
                        m5.getText().toString(),
                        m6.getText().toString(),
                        m7.getText().toString(),
                        m8.getText().toString(),
                        m9.getText().toString(),
                        m10.getText().toString(),
                        fct.getText().toString(),
                        bonificacion);

                totalPrice.setText(String.valueOf(matricula.calPrecioMatricula()));
            }
        });
    }
}
