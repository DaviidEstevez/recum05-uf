package com.example.recuperacionm05;

import java.util.ArrayList;
import java.util.List;

public class Matricula {
    public final int PREICO_UF = 30;
    public List<String> stringListaUfs = new ArrayList<>();
    public String fct;
    public double bonificacion;

    public Matricula(String m2,  String m3,  String m5, String m6, String m7, String m8,  String m9, String m10,  String fct, double bonificacion) {
        stringListaUfs.add(m2);
        stringListaUfs.add(m3);
        stringListaUfs.add(m5);
        stringListaUfs.add(m6);
        stringListaUfs.add(m7);
        stringListaUfs.add(m8);
        stringListaUfs.add(m9);
        stringListaUfs.add(m10);
        this.fct = fct;
        this.bonificacion = bonificacion;
    }

    public double calPrecioMatricula(){

        double totalPrice = this.PREICO_UF * sumaUfs() ;

        if(totalPrice > 360){

            totalPrice = 360;
        }

        return totalPrice * this.bonificacion;
    }

    public int sumaUfs(){

        int totalUfs = 0;

        for(String materia : stringListaUfs){

            if(!materia.isEmpty()){

                totalUfs +=  Integer.parseInt(materia);

            }

        }

        if(!fct.isEmpty() && totalUfs > 0){

            totalUfs += Integer.parseInt(fct);
        }


        return totalUfs;
    }
}
